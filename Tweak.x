@interface LSResourceProxy : NSObject @end
@interface LSDocumentProxy : LSResourceProxy
+(LSDocumentProxy*)documentProxyForName:(NSString*)name type:(NSString*)UTI MIMEType:(NSString*)MIME;
@end

@interface FluidProgressView : UIView
-(CGRect)_progressBarBoundsForValue:(double)value;
@end

@interface NavigationBarItem : NSObject
@property(assign) FluidProgressView* progressView;
@end

@interface TabDocument : NSObject
@property(assign) NSString* customUserAgent;
-(NavigationBarItem*)navigationBarItem;
@end

@interface TabController : NSObject
@property(assign) TabDocument* activeTabDocument;
-(NSArray*)currentTabDocuments;
@end

@interface WKPreferences : NSObject
@property(assign) BOOL javaScriptEnabled;
@end

@interface BrowserController : NSObject
@property(assign) id browserPanel;
@property(assign) BOOL usesNarrowLayout;
+(BrowserController*)sharedBrowserController;
-(void)closeActiveTabKeyPressed;
-(void)findOnPageCompletionProvider:(id)provider setStringToComplete:(NSString*)string;
-(BOOL)hideBrowserPanelType:(int)type;
-(void)newTabKeyPressed;
-(BOOL)showBrowserPanelType:(int)type;
-(void)togglePrivateBrowsing;
-(TabController*)tabController;
-(WKPreferences*)wkPreferences;
@end

@interface UIColor (Private)
+(UIColor*)systemOrangeColor;
+(UIColor*)selectionHighlightColor;
+(UIColor*)tableCellBlueTextColor;
+(UIColor*)tableCellGrayTextColor;
@end

@interface UIImage (Private)
+(UIImage*)kitImageNamed:(NSString*)name;
+(UIImage*)_iconForResourceProxy:(LSResourceProxy*)proxy variant:(int)variant variantsScale:(CGFloat)scale;
-(UIImage*)_flatImageWithColor:(UIColor*)color;
@end

@interface UITableViewCellLayoutManager : NSObject @end
@interface EditableTableViewCellLayoutManager : UITableViewCellLayoutManager
+(EditableTableViewCellLayoutManager*)sharedLayoutManager;
@end

@interface UITableViewCell (Private)
@property(assign) UITableViewCellLayoutManager* layoutManager;
-(UITextField*)editableTextField;
@end

static WKPreferences* _getWKPreferences(BrowserController* browser) {
  return [browser respondsToSelector:@selector(wkPreferences)]?
   browser.wkPreferences:nil;
}

// custom gestures
#import <UIKit/UIGestureRecognizerSubclass.h>
#define CSUserAgentNamesKey @"CSUserAgentNames"
#define CSUserAgentStringsKey @"CSUserAgentStrings"
#define FindOnPagePanelType 7

enum CSGestureAction {
  kCSGestureActionNull,
  kCSGestureActionCloseTab,
  kCSGestureActionFindOnPage,
  kCSGestureActionNewTab,
  kCSGestureActionNextTab,
  kCSGestureActionPreviousTab,
  kCSGestureActionSetUserAgent,
  kCSGestureActionToggleJavaScript,
  kCSGestureActionTogglePrivate
};

@interface CSGestureRecognizer : UIGestureRecognizer {
  UIView* _hudView;
  UIImageView* _hudIcon;
  UILabel* _hudLabel;
  CGPoint _origin;
  enum CSGesturePhase {
    kCSGesturePhaseNull,
    kCSGesturePhase0,
    kCSGesturePhase1,
    kCSGesturePhase2,
    kCSGesturePhase3,
  } _phase;
  unsigned short _nremain;
  BOOL _negY,_negX;
}
@end
@implementation CSGestureRecognizer
-(BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer*)gesture {
  return NO;
}
-(enum CSGestureAction)action {
  enum CSGesturePhase phase=_phase;
  return (phase==(_nremain?kCSGesturePhase3:kCSGesturePhase2))?_negY?
   _negX?kCSGestureActionFindOnPage:kCSGestureActionNewTab:
   _negX?kCSGestureActionNextTab:kCSGestureActionPreviousTab:
   (phase==kCSGesturePhase3)?_negY?
   _negX?kCSGestureActionTogglePrivate:kCSGestureActionCloseTab:
   _negX?kCSGestureActionToggleJavaScript:kCSGestureActionSetUserAgent:
   kCSGestureActionNull;
}
-(void)reset {
  [super reset];
  _phase=kCSGesturePhaseNull;
  UIView* hudView=_hudView;
  [UIView animateWithDuration:0.1 animations:^{
    hudView.transform=CGAffineTransformMakeScale(0.75,0.75);
    hudView.alpha=0;
  } completion:^(BOOL finished){[hudView removeFromSuperview];}];
}
-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event {
  [super touchesBegan:touches withEvent:event];
  if(_phase==kCSGesturePhaseNull && touches.count==1){
    _origin=[touches.anyObject locationInView:self.view];
    _phase=kCSGesturePhase0;
  }
  else {self.state=UIGestureRecognizerStateFailed;}
}
-(void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event {
  [super touchesMoved:touches withEvent:event];
  UIView* view=self.view;
  CGPoint current=[touches.anyObject locationInView:view];
  CGFloat dx=current.x-_origin.x,dy=current.y-_origin.y;
  const CGFloat offset=25;
  BOOL vert,neg;
  if(dy<-offset){vert=YES;neg=YES;}
  else if(dy>offset){vert=YES;neg=NO;}
  else if(dx<-offset){vert=NO;neg=YES;}
  else if(dx>offset){vert=NO;neg=NO;}
  else {return;}
  const unsigned short minsteps=2;
  switch(_phase){
    case kCSGesturePhase0:
      if(!vert){goto __fail;}
      _phase=kCSGesturePhase1;
      _nremain=minsteps;
      _negY=neg;
      break;
    case kCSGesturePhase1:
      if(vert){
        if(neg!=_negY){goto __fail;}
        if(_nremain){--_nremain;}
        break;
      }
      if(_nremain){goto __fail;}
      _phase=kCSGesturePhase2;
      _nremain=minsteps;
      _negX=neg;
      break;
    case kCSGesturePhase2:
      if(!vert){
        if(neg!=_negX){goto __fail;}
        __step:if(_nremain && !--_nremain){
          const CGFloat w1=40,w2=100;
          UIImage* image;
          NSString* text;
          switch(self.action){
            case kCSGestureActionCloseTab:
              image=[UIImage kitImageNamed:@"UIButtonBarStop"];
              text=@"Close Tab";
              break;
            case kCSGestureActionFindOnPage:
              image=[UIImage kitImageNamed:@"UIButtonBarSearch"];
              text=@"Find Text";
              break;
            case kCSGestureActionNewTab:
              image=[UIImage kitImageNamed:@"UIButtonBarNew"];
              text=@"New Tab";
              break;
            case kCSGestureActionNextTab:
              image=[UIImage kitImageNamed:@"UIButtonBarArrowRight"];
              text=@"Switch Tab";
              break;
            case kCSGestureActionPreviousTab:
              image=[UIImage kitImageNamed:@"UIButtonBarArrowLeft"];
              text=@"Switch Tab";
              break;
            case kCSGestureActionSetUserAgent:
              image=[UIImage imageNamed:@"RequestDesktopSiteIcon"];
              text=@"User Agent";
              break;
            case kCSGestureActionToggleJavaScript:
              image=[UIImage kitImageNamed:_getWKPreferences(
               [%c(BrowserController) sharedBrowserController]).javaScriptEnabled?
               @"UIButtonBarPause":@"UIButtonBarPlay"];
              text=@"JavaScript";
              break;
            case kCSGestureActionTogglePrivate:
              image=[UIImage kitImageNamed:@"UITabBarContactsTemplateSelected"];
              text=@"Private";
              break;
            default:return;
          }
          UIView* hudView=_hudView;
          UIImageView* hudIcon;
          UILabel* hudLabel;
          if(hudView){
            hudIcon=_hudIcon;
            hudLabel=_hudLabel;
          }
          else {
            hudView=_hudView=[[UIView alloc] initWithFrame:CGRectMake(0,0,w1+w2,w1)];
            hudView.autoresizingMask=UIViewAutoresizingFlexibleTopMargin
             |UIViewAutoresizingFlexibleBottomMargin
             |UIViewAutoresizingFlexibleLeftMargin
             |UIViewAutoresizingFlexibleRightMargin;
            hudView.backgroundColor=[UIColor colorWithWhite:0.8 alpha:1];
            CALayer* layer=hudView.layer;
            layer.cornerRadius=5;
            layer.masksToBounds=YES;
            layer.borderWidth=1;
            layer.borderColor=[UIColor darkGrayColor].CGColor;
            hudIcon=_hudIcon=[[UIImageView alloc] init];
            [hudView addSubview:hudIcon];
            [hudIcon release];
            hudLabel=_hudLabel=[[UILabel alloc] initWithFrame:CGRectMake(w1,0,w2,w1)];
            hudLabel.backgroundColor=[UIColor colorWithWhite:0.9 alpha:1];
            hudLabel.font=[UIFont systemFontOfSize:16];
            hudLabel.textAlignment=NSTextAlignmentCenter;
            [hudView addSubview:hudLabel];
            [hudLabel release];
          }
          CGRect frame={.size=image.size};
          frame.origin.x=(w1-frame.size.width)/2;
          frame.origin.y=(w1-frame.size.height)/2;
          hudIcon.frame=frame;
          hudIcon.image=image;
          hudLabel.text=text;
          if(!hudView.superview){
            hudView.center=view.center;
            hudView.transform=CGAffineTransformMakeScale(0.75,0.75);
            hudView.alpha=0;
            [view addSubview:hudView];
            [UIView animateWithDuration:0.1 animations:^{
              hudView.transform=CGAffineTransformIdentity;
              hudView.alpha=1;
            } completion:NULL];
          }
        }
        break;
      }
      if(_nremain || neg==_negY){goto __fail;}
      _phase=kCSGesturePhase3;
      _nremain=minsteps;
      break;
    case kCSGesturePhase3:
      if(vert){
        if(neg==_negY){goto __fail;}
        goto __step;
      }
    default:__fail:
      self.state=UIGestureRecognizerStateFailed;
      return;
  }
  _origin=current;
}
-(void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event {
  [super touchesEnded:touches withEvent:event];
  self.state=((_phase==kCSGesturePhase2 && !_nremain) || _phase==kCSGesturePhase3)?
   UIGestureRecognizerStateRecognized:UIGestureRecognizerStateFailed;
}
-(void)touchesCancelled:(NSSet*)touches withEvent:(UIEvent*)event {
  [super touchesCancelled:touches withEvent:event];
  self.state=UIGestureRecognizerStateFailed;
}
-(void)dealloc {
  [_hudView release];
  [super dealloc];
}
@end

@protocol CSUserAgentDelegate <NSObject>
-(void)updateEntryAtIndexPath:(NSIndexPath*)ipath name:(NSString*)name string:(NSString*)string;
@end
@interface CSUserAgentDetailController : UITableViewController {
  id<CSUserAgentDelegate> _delegate;
  NSIndexPath* _indexPath;
  UITableViewCell* _nameCell;
  UITableViewCell* _stringCell;
}
@end
@implementation CSUserAgentDetailController
-(id)initWithDelegate:(id<CSUserAgentDelegate>)delegate indexPath:(NSIndexPath*)indexPath name:(NSString*)name string:(NSString*)string {
  if((self=[super initWithStyle:UITableViewStyleGrouped])){
    self.title=@"Details";
    _delegate=delegate;
    _indexPath=[indexPath retain];
    UITableViewCell* nameCell=_nameCell=[[UITableViewCell alloc] init];
    UITableViewCell* stringCell=_stringCell=[[UITableViewCell alloc] init];
    nameCell.layoutManager=stringCell.layoutManager
     =[%c(EditableTableViewCellLayoutManager) sharedLayoutManager];
    UITextField* fname=nameCell.editableTextField;
    UITextField* fstring=stringCell.editableTextField;
    fname.placeholder=@"Name";
    fname.text=name;
    fstring.placeholder=@"String";
    fstring.text=string;
    fstring.autocapitalizationType=UITextAutocapitalizationTypeNone;
    fstring.autocorrectionType=UITextAutocorrectionTypeNo;
    fstring.returnKeyType=fname.returnKeyType=UIReturnKeyNext;
    [fname addTarget:self action:@selector(returnKeyPressed:)
     forControlEvents:UIControlEventEditingDidEndOnExit];
    [fstring addTarget:self action:@selector(returnKeyPressed:)
     forControlEvents:UIControlEventEditingDidEndOnExit];
  }
  return self;
}
-(void)returnKeyPressed:(UITextField*)field {
  UITextField* fname=_nameCell.editableTextField;
  [(field==fname)?_stringCell.editableTextField:fname becomeFirstResponder];
}
-(void)didMoveToParentViewController:(UIViewController*)controller {
  [super didMoveToParentViewController:controller];
  if(controller){return;}
  [_delegate updateEntryAtIndexPath:_indexPath
   name:_nameCell.editableTextField.text
   string:_stringCell.editableTextField.text];
}
-(NSInteger)tableView:(UITableView*)view numberOfRowsInSection:(NSInteger)section {
  return 2;
}
-(UITableViewCell*)tableView:(UITableView*)view cellForRowAtIndexPath:(NSIndexPath*)ipath {
  return (ipath.row==0)?_nameCell:_stringCell;
}
-(void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [_nameCell.editableTextField becomeFirstResponder];
}
-(void)dealloc {
  [_indexPath release];
  [_nameCell release];
  [_stringCell release];
  [super dealloc];
}
@end

@interface CSUserAgentListController : UITableViewController <CSUserAgentDelegate> {
  TabDocument* _tabDocument;
  NSMutableArray* _uaNames;
  NSMutableArray* _uaStrings;
  NSString* _currentString;
  NSUInteger currentIndex;
}
@end
@implementation CSUserAgentListController
-(id)initWithTabDocument:(TabDocument*)tabDocument {
  if((self=[super initWithStyle:UITableViewStyleGrouped])){
    self.title=@"User Agent";
    _tabDocument=[tabDocument retain];
    NSUserDefaults* defaults=[NSUserDefaults standardUserDefaults];
    NSArray* names=[defaults arrayForKey:CSUserAgentNamesKey];
    NSArray* strings=[defaults arrayForKey:CSUserAgentStringsKey];
    _uaNames=names?names.mutableCopy:[[NSMutableArray alloc] init];
    _uaStrings=strings?strings.mutableCopy:[[NSMutableArray alloc] init];
    NSUInteger count=strings.count,ncount=names.count;
    if(ncount>count){[_uaNames removeObjectsInRange:NSMakeRange(count,ncount-count)];}
    else while(ncount<count){[_uaNames insertObject:@"?" atIndex:ncount++];}
    NSString* current=tabDocument.customUserAgent;
    if(current.length){
      _currentString=[current retain];
      currentIndex=[strings indexOfObject:current];
    }
  }
  return self;
}
-(void)dismiss {
  [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)save {
  NSUserDefaults* defaults=[NSUserDefaults standardUserDefaults];
  [defaults setObject:_uaNames forKey:CSUserAgentNamesKey];
  [defaults setObject:_uaStrings forKey:CSUserAgentStringsKey];
}
-(void)updateEntryAtIndexPath:(NSIndexPath*)ipath name:(NSString*)name string:(NSString*)string {
  BOOL save=NO,update=NO;
  NSUInteger namelen=name.length,stringlen=string.length;
  if(ipath){
    NSUInteger index=ipath.row;
    if(namelen && ![name isEqualToString:[_uaNames objectAtIndex:index]]){
      save=YES;
      [_uaNames replaceObjectAtIndex:index withObject:name];
      UITableViewCell* cell=[self.tableView cellForRowAtIndexPath:ipath];
      if(cell){cell.textLabel.text=name;}
    }
    if(stringlen && ![string isEqualToString:[_uaStrings objectAtIndex:index]]){
      save=YES;
      [_uaStrings replaceObjectAtIndex:index withObject:string];
      if(_currentString && index==currentIndex){update=YES;}
    }
  }
  else if(stringlen && (namelen || ![string isEqualToString:_currentString])){
    if((ipath=_currentString?(currentIndex==NSNotFound)?nil:
     [NSIndexPath indexPathForRow:currentIndex inSection:1]:
     [NSIndexPath indexPathForRow:0 inSection:0])){
      UITableViewCell* cell=[self.tableView cellForRowAtIndexPath:ipath];
      if(cell){cell.accessoryType=UITableViewCellAccessoryNone;}
    }
    if(namelen){
      save=YES;
      currentIndex=0;
      [_uaNames insertObject:name atIndex:0];
      [_uaStrings insertObject:string atIndex:0];
      [self.tableView insertRowsAtIndexPaths:
       [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:1]]
       withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    update=YES;
  }
  if(save){[self save];}
  if(update){
    [_currentString release];
    _tabDocument.customUserAgent=_currentString=[string retain];
  }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)view {
  return 2;
}
-(NSInteger)tableView:(UITableView*)view numberOfRowsInSection:(NSInteger)section {
  return (section==1)?_uaStrings.count:1;
}
-(UITableViewCell*)tableView:(UITableView*)view cellForRowAtIndexPath:(NSIndexPath*)ipath {
  UITableViewCell* cell=[view dequeueReusableCellWithIdentifier:@"Cell"]?:
   [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
   reuseIdentifier:@"Cell"] autorelease];
  NSString* text;
  BOOL selected;
  if(ipath.section==1){
    NSUInteger index=ipath.row;
    text=[_uaNames objectAtIndex:index];
    selected=(_currentString && index==currentIndex);
  }
  else {
    text=@"Default";
    selected=!_currentString;
  }
  cell.textLabel.text=text;
  cell.accessoryType=selected?UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
  return cell;
}
-(UITableViewCellEditingStyle)tableView:(UITableView*)view editingStyleForRowAtIndexPath:(NSIndexPath*)ipath {
  return (ipath.section==1)?UITableViewCellEditingStyleDelete:UITableViewCellEditingStyleInsert;
}
-(void)tableView:(UITableView*)view commitEditingStyle:(UITableViewCellEditingStyle)style forRowAtIndexPath:(NSIndexPath*)ipath {
  if(style==UITableViewCellEditingStyleDelete){
    NSUInteger index=ipath.row;
    if(_currentString && currentIndex==index){currentIndex=NSNotFound;}
    [_uaNames removeObjectAtIndex:index];
    [_uaStrings removeObjectAtIndex:index];
    [view deleteRowsAtIndexPaths:[NSArray arrayWithObject:ipath]
     withRowAnimation:UITableViewRowAnimationAutomatic];
    [self save];
  }
  else {
    CSUserAgentDetailController* sub=[[CSUserAgentDetailController alloc]
     initWithDelegate:self indexPath:nil name:nil string:_currentString];
    [self.navigationController pushViewController:sub animated:YES];
    [sub release];
  }
}
-(BOOL)tableView:(UITableView*)view canMoveRowAtIndexPath:(NSIndexPath*)ipath {
  return ipath.section==1;
}
-(NSIndexPath*)tableView:(UITableView*)view targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath*)ipath1 toProposedIndexPath:(NSIndexPath*)ipath2 {
  return (ipath2.section==1)?ipath2:ipath1;
}
-(void)tableView:(UITableView*)view moveRowAtIndexPath:(NSIndexPath*)ipath1 toIndexPath:(NSIndexPath*)ipath2 {
  NSUInteger index1=ipath1.row,index2=ipath2.row;
  if(index1!=index2){
    if(_currentString && currentIndex==index1){currentIndex=index2;}
    NSString* name=[[_uaNames objectAtIndex:index1] retain];
    [_uaNames removeObjectAtIndex:index1];
    [_uaNames insertObject:name atIndex:index2];
    [name release];
    NSString* string=[[_uaStrings objectAtIndex:index1] retain];
    [_uaStrings removeObjectAtIndex:index1];
    [_uaStrings insertObject:string atIndex:index2];
    [string release];
    [self save];
  }
}
-(void)tableView:(UITableView*)view didSelectRowAtIndexPath:(NSIndexPath*)ipath {
  [view deselectRowAtIndexPath:ipath animated:YES];
  BOOL list=(ipath.section==1);
  if(!self.editing){
    _tabDocument.customUserAgent=list?[_uaStrings objectAtIndex:ipath.row]:nil;
    [self dismiss];
  }
  else if(list){
    NSUInteger index=ipath.row;
    CSUserAgentDetailController* sub=[[CSUserAgentDetailController alloc]
     initWithDelegate:self indexPath:ipath name:[_uaNames objectAtIndex:index]
     string:[_uaStrings objectAtIndex:index]];
    [self.navigationController pushViewController:sub animated:YES];
    [sub release];
  }
}
-(void)viewDidLoad {
  self.tableView.allowsSelectionDuringEditing=YES;
  UINavigationItem* navitem=self.navigationItem;
  [navitem.leftBarButtonItem=[[UIBarButtonItem alloc]
   initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
   target:self action:@selector(dismiss)] release];
  navitem.rightBarButtonItem=self.editButtonItem;
}
-(void)dealloc {
  [_tabDocument release];
  [_uaNames release];
  [_uaStrings release];
  [_currentString release];
  [super dealloc];
}
@end

// improved FindOnPage
%hook FindOnPagePanel
@interface FindOnPagePanel : UIView
-(UITextField*)findTextField;
-(int)panelType;
-(void)setUsesNarrowLayout:(BOOL)narrow forceUpdateUI:(BOOL)update;
@end
-(void)willMoveToSuperview:(UIView*)view {
  %orig;
  if(!view){[self.findTextField resignFirstResponder];}
}
%end

@interface FindOnPageToolbar
+(UIView*)sharedToolbar;
-(UITextField*)inputField;
@end

%hook BrowserController
static void configureFindOnPageToolbar(BOOL alt) {
  static Ivar v_leftToolbar=NULL,v_nextItem,v_previousItem;
  Class c_FindOnPageToolbar=%c(FindOnPageToolbar);
  if(!v_leftToolbar){
    v_leftToolbar=class_getInstanceVariable(c_FindOnPageToolbar,"_leftToolbar");
    v_nextItem=class_getInstanceVariable(c_FindOnPageToolbar,"_nextButtonItem");
    v_previousItem=class_getInstanceVariable(c_FindOnPageToolbar,"_previousButtonItem");
  }
  UIView* view=[c_FindOnPageToolbar sharedToolbar];
  NSArray* origitems=objc_getAssociatedObject(view,&v_leftToolbar);
  if(alt==!origitems){
    UIToolbar* toolbar=object_getIvar(view,v_leftToolbar);
    if(alt){
      objc_setAssociatedObject(view,&v_leftToolbar,toolbar.items,
       OBJC_ASSOCIATION_RETAIN_NONATOMIC);
      UIBarButtonItem* spacer=[[UIBarButtonItem alloc]
       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
      spacer.width=15;
      toolbar.items=[NSArray arrayWithObjects:object_getIvar(view,v_previousItem),
       spacer,object_getIvar(view,v_nextItem),nil];
      [spacer release];
    }
    else {
      toolbar.items=origitems;
      objc_setAssociatedObject(view,&v_leftToolbar,nil,OBJC_ASSOCIATION_ASSIGN);
    }
  }
}
%new(v@:@)
-(void)CS_collapseFindOnPagePanel {
  if(self.usesNarrowLayout){
    CFRunLoopPerformBlock(CFRunLoopGetMain(),kCFRunLoopDefaultMode,
     ^{[self.browserPanel setUsesNarrowLayout:YES forceUpdateUI:NO];});
  }
}
%new(v@:@)
-(void)CS_handleGesture:(id)_gesture {
  if([_gesture isMemberOfClass:[UITapGestureRecognizer class]]) __showFindOnPageToolbar:{
    FindOnPagePanel* panel=self.browserPanel;
    if(panel.panelType!=FindOnPagePanelType){
      [self findOnPageCompletionProvider:nil setStringToComplete:nil];
      [self showBrowserPanelType:FindOnPagePanelType];
      if((panel=self.browserPanel).panelType!=FindOnPagePanelType){return;}
    }
    BOOL alt=self.usesNarrowLayout;
    configureFindOnPageToolbar(alt);
    if(alt){[panel setUsesNarrowLayout:NO forceUpdateUI:NO];}
    else {[panel becomeFirstResponder];}
    [panel.findTextField becomeFirstResponder];
    return;
  }
  CSGestureRecognizer* gesture=_gesture;
  int switchtab;
  switch(gesture.action){
    case kCSGestureActionCloseTab:[self closeActiveTabKeyPressed];return;
    case kCSGestureActionFindOnPage:goto __showFindOnPageToolbar;
    case kCSGestureActionNewTab:[self newTabKeyPressed];return;
    case kCSGestureActionNextTab:switchtab=1;break;
    case kCSGestureActionPreviousTab:switchtab=-1;break;
    case kCSGestureActionSetUserAgent:{
      static Ivar var=NULL;
      if(!var){var=class_getInstanceVariable(%c(BrowserController),"_rootViewController");}
      CSUserAgentListController* sub=[[CSUserAgentListController alloc]
       initWithTabDocument:self.tabController.activeTabDocument];
      UINavigationController* nav=[[UINavigationController alloc]
       initWithRootViewController:sub];
      [sub release];
      [object_getIvar(self,var) presentViewController:nav animated:YES completion:NULL];
      [nav release];
      return;
    }
    case kCSGestureActionToggleJavaScript:{
      WKPreferences* prefs=_getWKPreferences(self);
      prefs.javaScriptEnabled=!prefs.javaScriptEnabled;
      return;
    }
    case kCSGestureActionTogglePrivate:[self togglePrivateBrowsing];return;
    default:return;
  }
  TabController* controller=self.tabController;
  NSArray* tabs=controller.currentTabDocuments;
  NSUInteger count=tabs.count;
  if(count>1){
    NSUInteger index=[tabs indexOfObjectIdenticalTo:controller.activeTabDocument];
    if(index!=NSNotFound){
      if(index==0 && switchtab==-1){index=count-1;}
      else if(index==count-1 && switchtab==1){index=0;}
      else {index+=switchtab;}
      controller.activeTabDocument=[tabs objectAtIndex:index];
    }
  }
}
-(void)_initSubviews {
  %orig;
  static Ivar var=NULL;
  if(!var){var=class_getInstanceVariable(%c(BrowserController),"_pageView");}
  UIGestureRecognizer* gesture=[[CSGestureRecognizer alloc]
   initWithTarget:self action:@selector(CS_handleGesture:)];
  [object_getIvar(self,var) addGestureRecognizer:gesture];
  [gesture release];
}
-(void)switchFromTabDocument:(TabDocument*)tabDocument1 toTabDocument:(TabDocument*)tabDocument2 {
  [self hideBrowserPanelType:FindOnPagePanelType];
  %orig;
}
-(void)_updateFindOnPagePanelIfNeeded {
  configureFindOnPageToolbar(NO);
  %orig;
}
%end

%hook FindOnPageToolbar
-(id)initWithFrame:(CGRect)frame {
  if((self=%orig)){
    UITextField* field=self.inputField;
    [field addTarget:[%c(BrowserController) sharedBrowserController]
     action:@selector(CS_collapseFindOnPagePanel)
     forControlEvents:UIControlEventEditingDidEnd];
    [field addTarget:field action:@selector(resignFirstResponder)
     forControlEvents:UIControlEventEditingDidEndOnExit];
  }
  return self;
}
%end

%hook FindOnPageResultsLabel
@interface FindOnPageResultsLabel : UILabel @end
-(id)initWithFrame:(CGRect)frame {
  if((self=%orig)){
    self.userInteractionEnabled=YES;
    UITapGestureRecognizer* gesture=[[UITapGestureRecognizer alloc]
     initWithTarget:[%c(BrowserController) sharedBrowserController]
     action:@selector(CS_handleGesture:)];
    [self addGestureRecognizer:gesture];
    [gesture release];
  }
  return self;
}
%end

// make bookmark address field editable
@interface CS_UITextField : UITextField @end
@implementation CS_UITextField
-(void)setEnabled:(BOOL)enabled {
  if(enabled){[super setEnabled:enabled];}
}
@end

%hook BookmarkTextEntryTableViewCell
@interface BookmarkTextEntryTableViewCell : UITableViewCell
-(UITextField*)editableTextField;
@end
-(id)initWithText:(NSString*)text autocapitalizationType:(UITextAutocapitalizationType)autocapitalizationType autocorrectionType:(UITextAutocorrectionType)autocorrectionType {
  if((self=%orig)){object_setClass(self.editableTextField,[CS_UITextField class]);}
  return self;
}
%end

// swap history with shared links
%hook BookmarksNavigationController
-(void)_setupCollections {
  %orig;
  static Ivar v_collectionsParentedInBookmarks=NULL,v_topLevelCollections;
  if(!v_collectionsParentedInBookmarks){
    Class cls=%c(BookmarksNavigationController);
    v_collectionsParentedInBookmarks=class_getInstanceVariable(cls,"_collectionsParentedInBookmarks");
    v_topLevelCollections=class_getInstanceVariable(cls,"_topLevelCollections");
  }
  NSArray* parented=object_getIvar(self,v_collectionsParentedInBookmarks);
  if([parented containsObject:@"HistoryCollection"]){
    NSArray* top=object_getIvar(self,v_topLevelCollections);
    NSUInteger ntop=[top indexOfObject:@"SocialLinksCollection"];
    NSArray* remainder;
    if(ntop==NSNotFound){ntop=top.count;remainder=nil;}
    else {remainder=[top subarrayWithRange:NSMakeRange(ntop,top.count-ntop)];}
    const NSUInteger nparented=parented.count,ntotal=ntop+nparented;
    NSString** collections=malloc(sizeof(NSString*)*ntotal);
    [top getObjects:collections range:NSMakeRange(0,ntop)];
    [parented getObjects:collections+ntop range:NSMakeRange(0,nparented)];
    object_setIvar(self,v_topLevelCollections,
     [NSArray arrayWithObjects:collections count:ntotal]);
    free(collections);
    object_setIvar(self,v_collectionsParentedInBookmarks,remainder);
  }
}
-(UIImage*)_segmentedControlItemForCollection:(NSString*)collection {
  return [collection isEqualToString:@"HistoryCollection"]?
   [UIImage imageNamed:@"BookmarksListHistoryFolder"]:%orig;
}
%end

%hook BookmarksTableViewController
-(UIImage*)_grayedOutHistoryFolderIcon {
  NSString* const name=@"SocialLinksIcon";
  UIImage* image=objc_getAssociatedObject(self,name);
  if(!image){
    objc_setAssociatedObject(self,name,image=[[UIImage imageNamed:name]
     _flatImageWithColor:[UIColor grayColor]],OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  }
  return image;
}
-(UIImage*)_tintedImageNamed:(NSString*)name {
  return %orig([name isEqualToString:@"BookmarksListHistoryFolder"]?
   @"SocialLinksIcon":name);
}
%end

// download media files on refresh
#import <MobileCoreServices/UTType.h>
#import <MobileCoreServices/UTCoreTypes.h>
%hook WKNavigationResponse
@interface WKNavigationResponse
-(NSURLRequest*)_request;
-(NSURLResponse*)response;
@end
-(void)setCanShowMIMEType:(BOOL)show {
  if(show && [self._request valueForHTTPHeaderField:@"Cache-Control"]){
    CFStringRef UTI=UTTypeCreatePreferredIdentifierForTag(
     kUTTagClassMIMEType,(CFStringRef)self.response.MIMEType,NULL);
    if(UTI){
      if(UTTypeConformsTo(UTI,kUTTypeAudiovisualContent)){show=NO;}
      CFRelease(UTI);
    }
  }
  %orig;
}
%end

// display download progress
@interface CSLoadingControllerProxy : NSObject {
  UIView* _progressView;
  NavigationBarItem* _barItem;
  FluidProgressView* _liveFPView;
  FluidProgressView* _origFPView;
}
@property(readonly,nonatomic) id target;
@end
@implementation CSLoadingControllerProxy
@synthesize target=_target;
-(id)initWithTarget:(id)target navigationBarItem:(NavigationBarItem*)barItem {
  if((self=[super init])){
    _target=[target retain];
    static Class c_FluidProgressView=nil;
    static Ivar v_clippingView,v_progressBar;
    if(!c_FluidProgressView){
      c_FluidProgressView=objc_getClass("FluidProgressView");
      v_clippingView=class_getInstanceVariable(c_FluidProgressView,"_clippingView");
      v_progressBar=class_getInstanceVariable(c_FluidProgressView,"_progressBar");
    }
    FluidProgressView* fpv=[[c_FluidProgressView alloc] init];
    [object_getIvar(fpv,v_progressBar) removeFromSuperview];
    (_progressView=object_getIvar(fpv,v_clippingView))
     .backgroundColor=[UIColor systemOrangeColor];
    _barItem=[barItem retain];
    _origFPView=[barItem.progressView retain];
    _liveFPView=barItem.progressView=fpv;
  }
  return self;
}
-(void)setEstimatedProgress:(float)progress {
  _progressView.bounds=[_liveFPView _progressBarBoundsForValue:progress];
}
-(NSMethodSignature*)methodSignatureForSelector:(SEL)cmd {
  return [_target methodSignatureForSelector:cmd];
}
-(id)forwardingTargetForSelector:(SEL)cmd {
  return _target;
}
-(void)dealloc {
  NavigationBarItem* barItem=_barItem;
  [barItem.progressView=_origFPView release];
  [barItem release];
  [_liveFPView release];
  [_target release];
  [super dealloc];
}
@end

static Ivar v_loadingController;
%hook TabDocument
-(void)_beginDownloadBackgroundTask:(id)download {
  %orig;
  CSLoadingControllerProxy* proxy=[[CSLoadingControllerProxy alloc]
   initWithTarget:object_getIvar(self,v_loadingController)
   navigationBarItem:self.navigationBarItem];
  object_setIvar(self,v_loadingController,proxy);
}
-(void)_endDownloadBackgroundTask {
  %orig;
  CSLoadingControllerProxy* proxy=object_getIvar(self,v_loadingController);
  if([proxy isKindOfClass:[CSLoadingControllerProxy class]]){
    object_setIvar(self,v_loadingController,proxy.target);
    [proxy release];
  }
}
%end

// upload files
@interface CSFilePickerEntry : NSObject
@property(readonly,nonatomic) NSString* title;
@property(readonly,nonatomic) NSAttributedString* subtitle;
@property(readonly,nonatomic) UIImage* icon;
@property(readonly,nonatomic) NSURL* URL;
@property(readonly,nonatomic) BOOL isSymlink;
@end
@implementation CSFilePickerEntry
@synthesize title=_title;
@synthesize subtitle=_subtitle;
@synthesize icon=_icon;
@synthesize URL=_URL;
@synthesize isSymlink=_isSymlink;
-(id)initWithURL:(NSURL*)URL isDirectory:(BOOL)isDirectory {
  if((self=[super init])){
    NSString* title=_title=[URL.lastPathComponent retain];
    UIColor* mdatecolor=[UIColor tableCellGrayTextColor];
    NSDate* mdate;
    NSString* mdatestr=[URL getResourceValue:&mdate forKey:NSURLContentModificationDateKey
     error:NULL]?[NSDateFormatter localizedStringFromDate:mdate
     dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle]:@"???";
    if(isDirectory){
      _subtitle=[[NSAttributedString alloc] initWithString:mdatestr
       attributes:[NSDictionary dictionaryWithObject:mdatecolor
       forKey:NSForegroundColorAttributeName]];
    }
    else {
      NSString* fsizestr;
      NSNumber* fsize;
      if([URL getResourceValue:&fsize forKey:NSURLFileSizeKey error:NULL]){
        unsigned long long bytes=fsize.unsignedLongLongValue;
        if(bytes<1024){fsizestr=[NSString localizedStringWithFormat:@"%llu B",bytes];}
        else {
          float kbytes=(float)bytes/1024;
          int i;for (i=0;i<3 && kbytes>=1024;i++){kbytes/=1024;}
          fsizestr=[NSString localizedStringWithFormat:@"%.1f %cB",
           kbytes,((char[]){'K','M','G','T'})[i]];
        }
      }
      else {fsizestr=@"???";}
      NSString* str=[NSString stringWithFormat:@"%@ \u2022 %@",fsizestr,mdatestr];
      NSMutableAttributedString* subtitle;
      _subtitle=subtitle=[[NSMutableAttributedString alloc] initWithString:str];
      [subtitle beginEditing];
      [subtitle addAttribute:NSForegroundColorAttributeName
       value:[UIColor tableCellBlueTextColor] range:NSMakeRange(0,fsizestr.length)];
      NSUInteger mdatelen=mdatestr.length;
      [subtitle addAttribute:NSForegroundColorAttributeName
       value:mdatecolor range:NSMakeRange(str.length-mdatelen,mdatelen)];
      [subtitle endEditing];
      _icon=[[UIImage _iconForResourceProxy:[LSDocumentProxy
       documentProxyForName:title type:nil MIMEType:nil]
       variant:0 variantsScale:2] retain];
    }
    _URL=[URL retain];
    NSNumber* value;
    _isSymlink=([URL getResourceValue:&value forKey:NSURLIsSymbolicLinkKey
     error:NULL] && value.boolValue);
  }
  return self;
}
-(NSComparisonResult)compare:(CSFilePickerEntry*)entry {
  return [_title localizedStandardCompare:entry.title];
}
-(void)dealloc {
  [_title release];
  [_subtitle release];
  [_icon release];
  [_URL release];
  [super dealloc];
}
@end

static void loadContents(NSURL* URL,NSMutableArray* files,NSMutableArray* folders) {
  NSFileManager* manager=[NSFileManager defaultManager];
  for (NSURL* subURL in [manager enumeratorAtURL:URL includingPropertiesForKeys:
   [NSArray arrayWithObjects:NSURLFileSizeKey,NSURLContentModificationDateKey,
   NSURLIsSymbolicLinkKey,nil] options:NSDirectoryEnumerationSkipsHiddenFiles
   |NSDirectoryEnumerationSkipsSubdirectoryDescendants errorHandler:nil]){
    BOOL isDirectory;
    if([manager fileExistsAtPath:subURL.path isDirectory:&isDirectory]){
      CSFilePickerEntry* entry=[[CSFilePickerEntry alloc]
       initWithURL:subURL isDirectory:isDirectory];
      [isDirectory?folders:files addObject:entry];
      [entry release];
    }
  }
  [files sortUsingSelector:@selector(compare:)];
  [folders sortUsingSelector:@selector(compare:)];
}
static void setSelected(UITableViewCell* cell,BOOL selected) {
  cell.accessoryType=selected?UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
  cell.backgroundColor=selected?[UIColor selectionHighlightColor]:nil;
}
@interface WKFileUploadPanel : UIViewController
-(void)_cancel;
-(void)_chooseFiles:(NSArray*)URLs displayString:(NSString*)string iconImage:(UIImage*)image;
@end
@interface CSFilePicker : UITableViewController {
  NSMutableArray* _files;
  NSMutableArray* _folders;
  NSMutableSet* _selectedFiles;
  NSURL* _URL;
  UIBarButtonItem* _cancelItem;
  UIBarButtonItem* _deselectItem;
  UIBarButtonItem* _submitItem;
  WKFileUploadPanel* _panel;
  BOOL _multiple;
}
@end
@implementation CSFilePicker
-(id)initWithURL:(NSURL*)URL panel:(WKFileUploadPanel*)panel multiple:(BOOL)multiple {
  if((self=[super initWithStyle:UITableViewStylePlain])){
    loadContents(_URL=[URL.URLByResolvingSymlinksInPath retain],
     _files=[[NSMutableArray alloc] init],
     _folders=[[NSMutableArray alloc] init]);
    _selectedFiles=[[NSMutableSet alloc] init];
    self.navigationItem.rightBarButtonItem=_cancelItem=[[UIBarButtonItem alloc]
     initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
     target:self action:@selector(dismissWithItem:)];
    _deselectItem=[[UIBarButtonItem alloc]
     initWithBarButtonSystemItem:UIBarButtonSystemItemStop
     target:self action:@selector(deselectAll)];
    _submitItem=[[UIBarButtonItem alloc]
     initWithBarButtonSystemItem:UIBarButtonSystemItemDone
     target:self action:@selector(dismissWithItem:)];
    _panel=panel;
    _multiple=multiple;
    UIRefreshControl* refresh=[[UIRefreshControl alloc] init];
    [refresh addTarget:self action:@selector(reload)
     forControlEvents:UIControlEventValueChanged];
    [self.refreshControl=refresh release];
    self.title=URL.lastPathComponent;
  }
  return self;
}
-(void)deselectAll {
  UITableView* view=self.tableView;
  for (NSIndexPath* ipath in view.indexPathsForVisibleRows){
    if(ipath.section && [_selectedFiles containsObject:[_files objectAtIndex:ipath.row]]){
      setSelected([view cellForRowAtIndexPath:ipath],NO);
    }
  }
  [_selectedFiles removeAllObjects];
  UINavigationItem* navitem=self.navigationItem;
  navitem.leftBarButtonItem=nil;
  navitem.rightBarButtonItem=_cancelItem;
}
-(void)dismissWithItem:(UIBarButtonItem*)item {
  WKFileUploadPanel* panel=[_panel retain];
  if(item==_submitItem){
    NSSet* selectedFiles=_selectedFiles;
    NSURL** URLs=malloc(selectedFiles.count*sizeof(NSURL*));
    int i=0;
    for (CSFilePickerEntry* entry in selectedFiles){URLs[i++]=entry.URL;}
    [panel _chooseFiles:[NSArray arrayWithObjects:URLs count:i]
     displayString:(i>1)?[NSString localizedStringWithFormat:
     [[NSBundle bundleWithIdentifier:@"com.apple.WebCore"]
     localizedStringForKey:@"%d files" value:nil table:nil],i]:
     URLs[0].lastPathComponent iconImage:nil];
    free(URLs);
  }
  else {[panel _cancel];}
  [panel release];
  [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)reload {
  [self deselectAll];
  NSMutableArray* files=_files;
  NSMutableArray* folders=_folders;
  [files removeAllObjects];
  [folders removeAllObjects];
  loadContents(_URL,files,folders);
  [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0,2)]
   withRowAnimation:UITableViewRowAnimationAutomatic];
  [self.refreshControl endRefreshing];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)view {
  return 2;
}
-(NSInteger)tableView:(UITableView*)view numberOfRowsInSection:(NSInteger)section {
  return (section?_files:_folders).count;
}
-(UITableViewCell*)tableView:(UITableView*)view cellForRowAtIndexPath:(NSIndexPath*)ipath {
  NSUInteger section=ipath.section;
  NSString* reuseID=section?@"FileCell":@"FolderCell";
  UITableViewCell* cell=[view dequeueReusableCellWithIdentifier:reuseID];
  if(!cell){
    cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
     reuseIdentifier:reuseID] autorelease];
    if(!section){
      cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
      cell.imageView.image=[UIImage imageNamed:@"FavoritesGridFolder"];
    }
  }
  CSFilePickerEntry* entry=[section?_files:_folders objectAtIndex:ipath.row];
  UILabel* label=cell.textLabel;
  label.text=entry.title;
  label.textColor=entry.isSymlink?[UIColor blueColor]:nil;
  cell.detailTextLabel.attributedText=entry.subtitle;
  if(section){
    cell.imageView.image=entry.icon;
    setSelected(cell,[_selectedFiles containsObject:entry]);
  }
  return cell;
}
-(void)tableView:(UITableView*)view didSelectRowAtIndexPath:(NSIndexPath*)ipath {
  [view deselectRowAtIndexPath:ipath animated:YES];
  NSUInteger section=ipath.section;
  CSFilePickerEntry* entry=[section?_files:_folders objectAtIndex:ipath.row];
  if(section){
    BOOL select=![_selectedFiles containsObject:entry];
    if(select){
      [_selectedFiles addObject:entry];
      if(!_multiple){
        [self dismissWithItem:_submitItem];
        return;
      }
    }
    else {[_selectedFiles removeObject:entry];}
    setSelected([view cellForRowAtIndexPath:ipath],select);
    UINavigationItem* navitem=self.navigationItem;
    NSUInteger nselected=_selectedFiles.count;
    navitem.leftBarButtonItem=nselected?_deselectItem:nil;
    navitem.rightBarButtonItem=nselected?_submitItem:_cancelItem;
  }
  else {
    [self deselectAll];
    CSFilePicker* picker=[[CSFilePicker alloc]
     initWithURL:entry.URL panel:_panel multiple:_multiple];
    [self showViewController:picker sender:nil];
    [picker release];
  }
}
-(void)dealloc {
  [_files release];
  [_folders release];
  [_selectedFiles release];
  [_URL release];
  [_cancelItem release];
  [_deselectItem release];
  [_submitItem release];
  [super dealloc];
}
@end

static SEL s_presentFullscreen;
static Ivar v_actionSheetController;
static ptrdiff_t x_allowMultipleFiles;
static void (*F_WKFileUploadPanel_presentFullscreen)(WKFileUploadPanel*,SEL,id,BOOL);
static void (*F_WKFileUploadPanel_presentPopover)(WKFileUploadPanel*,SEL,id,BOOL);
static void f_WKFileUploadPanel_present(WKFileUploadPanel* self,SEL _cmd,UIAlertController* alert,BOOL animated) {
  void (*orig)(id,SEL,id,BOOL)=(_cmd==s_presentFullscreen)?
   F_WKFileUploadPanel_presentFullscreen:F_WKFileUploadPanel_presentPopover;
  if([alert isKindOfClass:[UIAlertController class]]) __addAction:{
    BOOL multiple=*(BOOL*)((char*)self+x_allowMultipleFiles);
    [alert addAction:[UIAlertAction actionWithTitle:
     [[NSBundle bundleWithIdentifier:@"com.apple.WebCore"]
     localizedStringForKey:multiple?@"Choose Files":@"Choose File" value:nil table:nil]
     style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
      CSFilePicker* picker=[[CSFilePicker alloc] initWithURL:
       [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
       inDomains:NSUserDomainMask].lastObject panel:self multiple:multiple];
      UINavigationController* nav=[[UINavigationController alloc]
       initWithRootViewController:picker];
      [picker release];
      orig(self,_cmd,nav,animated);
      [nav release];
    }]];
  }
  else if(!object_getIvar(self,v_actionSheetController)){
    UIViewController* controller=alert;
    alert=[UIAlertController alertControllerWithTitle:nil
     message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:
     [NSString stringWithFormat:@"<%s>",object_getClassName(controller)]
     style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
     orig(self,_cmd,controller,animated);}]];
    if(_cmd==s_presentFullscreen){
      [alert addAction:[UIAlertAction actionWithTitle:
       [[NSBundle bundleWithIdentifier:@"com.apple.UIKit"]
       localizedStringForKey:@"Cancel" value:nil table:nil]
       style:UIAlertActionStyleCancel handler:^(UIAlertAction* action){[self _cancel];}]];
    }
    goto __addAction;
  }
  orig(self,_cmd,alert,animated);
}

// add StartPage search engine
@interface SearchEngineBasicInfo : NSObject @end
@interface SearchEngineInfo : SearchEngineBasicInfo
-(instancetype)initWithName:(NSString*)shortName scriptName:(NSString*)scriptName searchID:(int)searchID searchTemplate:(NSString*)searchTemplate homepageURLs:(NSArray*)homepageURLs hostSuffixes:(NSArray*)hostSuffixes pathPrefixes:(NSArray*)pathPrefixes usesSearchTermsFromFragment:(BOOL)usesSearchTermsFromFragment suggestionsTemplate:(NSString*)suggestionsTemplate topLevelDomains:(NSDictionary*)topLevelDomains safeSearchSuffix:(NSString*)safeSearchSuffix safeSearchURLQueryParameters:(NSDictionary*)safeSearchURLQueryParameters controller:(id)controller;
-(void)setSearchURL;
@end
static Class c_SearchEngineInfo;
static Ivar v_searchEngines;
static ptrdiff_t x_defaultSearchEngineIndex;
static void (*F_SearchEngineController_addSpecialSearchEngines)(id,SEL);
static void f_SearchEngineController_addSpecialSearchEngines(id self,SEL _cmd) {
  F_SearchEngineController_addSpecialSearchEngines(self,_cmd);
  SearchEngineInfo* info=[[c_SearchEngineInfo alloc]
   initWithName:@"Startpage" scriptName:@"Startpage" searchID:0
   searchTemplate:@"https://www.startpage.com/do/search?query={searchTerms}"
   homepageURLs:nil hostSuffixes:nil pathPrefixes:nil usesSearchTermsFromFragment:NO
   suggestionsTemplate:@"https://www.startpage.com/do/suggest?format=json&query={searchTerms}"
   topLevelDomains:nil safeSearchSuffix:nil safeSearchURLQueryParameters:nil controller:self];
  [info setSearchURL];
  NSMutableArray* engines=object_getIvar(self,v_searchEngines);
  [engines insertObject:info atIndex:*(NSUInteger*)((char*)self+x_defaultSearchEngineIndex)=engines.count];
  [info release];
}

// set base URL to unreachable URL when loading alternate HTML
%hook WKWebView
-(void)_loadAlternateHTMLString:(NSString*)html baseURL:(NSURL*)baseURL forUnreachableURL:(NSURL*)unreachableURL {
  baseURL=unreachableURL;
  %orig;
}
%end

%ctor {
  v_loadingController=class_getInstanceVariable(%c(TabDocument),"_loadingController");
  %init;
  Class c_WKFileUploadPanel=objc_getClass("WKFileUploadPanel");
  v_actionSheetController=class_getInstanceVariable(c_WKFileUploadPanel,"_actionSheetController");
  x_allowMultipleFiles=ivar_getOffset(class_getInstanceVariable(c_WKFileUploadPanel,"_allowMultipleFiles"));
  MSHookMessageEx(c_WKFileUploadPanel,
   s_presentFullscreen=@selector(_presentFullscreenViewController:animated:),
   (IMP)f_WKFileUploadPanel_present,(IMP*)&F_WKFileUploadPanel_presentFullscreen);
  MSHookMessageEx(c_WKFileUploadPanel,
   @selector(_presentPopoverWithContentViewController:animated:),
   (IMP)f_WKFileUploadPanel_present,(IMP*)&F_WKFileUploadPanel_presentPopover);
  Class c_SearchEngineController=objc_getClass("SearchEngineController");
  c_SearchEngineInfo=objc_getClass("SearchEngineInfo");
  v_searchEngines=class_getInstanceVariable(c_SearchEngineController,"_searchEngines");
  x_defaultSearchEngineIndex=ivar_getOffset(class_getInstanceVariable(c_SearchEngineController,"_defaultSearchEngineIndex"));
  MSHookMessageEx(c_SearchEngineController,@selector(_addSpecialSearchEngines),
   (IMP)f_SearchEngineController_addSpecialSearchEngines,
   (IMP*)&F_SearchEngineController_addSpecialSearchEngines);
  method_setImplementation(class_getInstanceMethod(c_SearchEngineInfo,@selector(displayName)),
   class_getMethodImplementation(c_SearchEngineInfo,@selector(scriptName)));
}
