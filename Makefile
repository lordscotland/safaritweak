ARCHS = arm64

include theos/makefiles/common.mk

TWEAK_NAME = SafariTweak
SafariTweak_FILES = Tweak.x
SafariTweak_FRAMEWORKS = CoreGraphics MobileCoreServices UIKit

include $(THEOS_MAKE_PATH)/tweak.mk
